import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.rnorth.ducttape.unreliables.Unreliables;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;

import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class KafkaTestService {

    /**
     * Возвращает кафкаПродюсера
     *
     * @param bootstrapServers
     * @return KafkaProducer
     */
    public static KafkaProducer<String, String> kafkaProducerBuilder(String bootstrapServers) {
        return new KafkaProducer<>(
                ImmutableMap.of(
                        ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                        bootstrapServers,
                        ProducerConfig.CLIENT_ID_CONFIG,
                        UUID.randomUUID().toString()
                ),
                new StringSerializer(),
                new StringSerializer()
        );
    }


    /**
     * Возвращает кафкаКонсумера
     *
     * @param bootstrapServers
     * @return KafkaProducer
     */
    public static   KafkaConsumer<String, String> kafkaConsumeBuilder(String bootstrapServers) {
        return new KafkaConsumer<>(
                ImmutableMap.of(
                        ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                        bootstrapServers,
                        ConsumerConfig.GROUP_ID_CONFIG,
                        "tc-" + UUID.randomUUID(),
                        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG,
                        "earliest"
                ),
                new StringDeserializer(),
                new StringDeserializer()
        );
    }

    public static AtomicInteger consume(KafkaConsumer<String, String> consumer, AtomicInteger atomicInteger) {
        Unreliables.retryUntilTrue(
                10,
                TimeUnit.SECONDS,
                () -> {

                    ConsumerRecords<String, String> records = consumer.poll(100L);

                    if (records.isEmpty()) {
                        return false;
                    }
                    for (var record : records) {
                        log.info(record.value());
                        atomicInteger.incrementAndGet();

                    }
                    return true;
                }
        );
        return atomicInteger;
    }
}
