import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.example.models.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Генератор сообщений + многопоточная отправка
 * сообщений в кафку
 */

public class Generator implements Runnable {
    private final String guruname;

    private final KafkaProducer<String, String> producer;

    private final String topicName;

    Generator(String name, KafkaProducer<String, String> producer, String topicName) {
        guruname = name;
        this.producer = producer;
        this.topicName = topicName;
    }

    /**
     * метод должне отправить в кафку 1000 сообщений в определеный
     * топик при запуске одного треда.
     *
     */
    @Override
    public void run() {
        for (int i = 0; i < 150; i++) {

            MessageModel messageModel = singleGenerator();

            try {
                producer.send(new ProducerRecord<>(topicName, "testcontainers", messageModel.toString())).get();
            } catch (InterruptedException | ExecutionException e) {
                throw new RuntimeException(e);
            }

        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Thread has been interrupted");
        }
    }


    /**
     * метод возвращает заполненный случайными данными
     * объект MessageModel
     *
     * @return MessageModel
     */
    private MessageModel singleGenerator() {
        return new MessageModel(
                UUID.randomUUID().toString(),
                LocalDateTime.now(),
                new Meta(
                        new Container(
                                Double.valueOf(getRandomStringElem(GenSourceEnum.CONTAINER_CAPACITIES.getValues())),
                                getRandomStringElem(GenSourceEnum.CONTAINER_MATERIALS.getValues()),
                                getRandomStringElem(GenSourceEnum.CONTAINER_DESC.getValues())
                        ),
                        new LiquidProperties(
                                getRandomStringElem(GenSourceEnum.LIQUID_DENS.getValues()),
                                Math.random() > 0.5,
                                getRandomStringElem(GenSourceEnum.LIQUID_DESC.getValues()),
                                new Components(
                                        getRandomStringElem(GenSourceEnum.MAIN_COMP_NAME.getValues()),
                                        getRandomStringElem(GenSourceEnum.SECOND_COMP_NAME.getValues()),
                                        getRandomStringElem(GenSourceEnum.THIRD_COMP_NAME.getValues())
                                )
                        )
                )
        );
    }
    /**
     * утилитарный метод, получает лист String,
     * возвращает рандомный элемент
     * а еще можно просто сделать list.get((int) (Math.random() * size))
     *
     * @return String
     */

    private String getRandomStringElem(List<String> stringList) {
        int size = stringList.size();
        int randomIndex = (int) (Math.random() * size);
        return stringList.get(randomIndex);
    }
}

