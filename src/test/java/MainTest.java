import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.rnorth.ducttape.unreliables.Unreliables;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.google.common.collect.ImmutableMap;
import org.testcontainers.utility.DockerImageName;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@Slf4j
@Testcontainers
public class MainTest {

    @Container
    KafkaContainer kafka = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

    @Test
    public void logTest() {
        System.out.println("asdfasdf");
        log.info("77777777");

        String FILENAME = "/file/does/not/exist";

        log.info("Just a log message.");
        log.debug("Message for debug level.");
        try {
            Files.readAllBytes(Paths.get(FILENAME));
        } catch (IOException ioex) {
            log.error("Failed to read file {}.", FILENAME, ioex);
        }

    }

    @Test
    public void kafkaTest() throws Exception {

        ExecutorService executorService = Executors.newFixedThreadPool(5);
        // запустили кафку
        String bootstrapServers = kafka.getBootstrapServers();
        kafka.start();
        log.info(bootstrapServers);

        //создали консумера и продюсера
        try (
                AdminClient adminClient = AdminClient.create(
                        ImmutableMap.of(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG,
                        bootstrapServers));

                KafkaProducer<String, String> producer = KafkaTestService.kafkaProducerBuilder(bootstrapServers);
                KafkaConsumer<String, String> consumer = KafkaTestService.kafkaConsumeBuilder(bootstrapServers)
        ) {

            // задали имя топика
            String topicName = "multiThreadMessageTopic";

            // создали топик с одной партицией и rf 1 (replication factor)
            Collection<NewTopic> topics
                    = Collections.singletonList(new NewTopic(topicName, 1, (short) 1));
            adminClient.createTopics(topics).all().get(30, TimeUnit.SECONDS);

            // подписали консумера на топик multiThreadMessageTopic
            consumer.subscribe(Collections.singletonList(topicName));

            // поднимаем два треда и пишем ими в кафку...
            Generator generator1 = new Generator("Gen1", producer, topicName);
            Generator generator2 = new Generator("Gen2", producer, topicName);
            Generator generator3 = new Generator("Gen3", producer, topicName);


            executorService.submit(generator1);
            executorService.submit(generator2);
            executorService.submit(generator3);

            TimeUnit.SECONDS.sleep(5);

            AtomicInteger atomicInteger = new AtomicInteger(0);

            atomicInteger =  KafkaTestService.consume(consumer, atomicInteger);

            consumer.unsubscribe();
            executorService.shutdown();

            log.info("executorService.awaitTermination = " + executorService.awaitTermination(1, TimeUnit.MINUTES));
            log.info("nums of records " + atomicInteger.toString());

            Assertions.assertEquals(atomicInteger.toString(), "450");
        }
    }



}
