package org.example.models;

public record Components(
        // light water,
        // holy water,
        // heavy water,
        // tritium-deuterium water,
        // tritium-protium water
        String mainComponent,
        // Hydrogen cyanide, Mustard gas, Phosgene, Lewisite, Adamsite, Beef liver
        String secondaryComponent,
        // curcumin, saffron, carmine, chlorophyll
        String tertiaryComponent) {
}
