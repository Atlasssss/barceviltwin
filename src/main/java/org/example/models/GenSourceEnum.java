package org.example.models;

import lombok.Getter;

import java.util.List;

@Getter
public enum GenSourceEnum {

    CONTAINER_CAPACITIES(List.of("0.2", "0.5", "0.7", "1")),
    CONTAINER_MATERIALS(List.of("tungsten", "steel", "mithril", "gold", "platinum")),
    CONTAINER_DESC(List.of("bottle", "can", "barrel", "beaker")),

    LIQUID_DENS(List.of("dense", "light", "undecided")),
    LIQUID_DESC(List.of("Lollygag", "Hoodwink", "Skedaddle", "Shenanigans", "Ragamuffin")),

    MAIN_COMP_NAME(List.of("light water", "holy water", "heavy water", "tritium-deuterium water", "tritium-protium water")),
    SECOND_COMP_NAME(List.of("Hydrogen cyanide", "Mustard gas", "Phosgene", "Lewisite", "Adamsite", "Beef liver")),
    THIRD_COMP_NAME(List.of("curcumin", "saffron", "carmine", "chlorophyll"));

    GenSourceEnum(List<String> values) {
        this.values = values;
    }

    final List<String> values;
}
