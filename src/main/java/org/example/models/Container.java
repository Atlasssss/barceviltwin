package org.example.models;

public record Container(
        //0.2, 0.5, 0.7, 1
        Double capacity,
        //tungsten, steel, mithril, gold, platinum
        String material,
        //bottle, can, barrel, beaker
        String description) {
}
