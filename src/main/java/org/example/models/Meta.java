package org.example.models;

public record Meta(
        Container container,
        LiquidProperties properties) {

}
