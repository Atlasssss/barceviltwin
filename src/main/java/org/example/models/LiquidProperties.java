package org.example.models;

public record LiquidProperties(
        //dense, light, undecided
        String density,
        Boolean isFlammable,
        // Lollygag, Hoodwink, Skedaddle, Shenanigans, Ragamuffin
        String liquidDescription,
        Components components) {

}
