package org.example.models;

import java.time.LocalDateTime;

public record MessageModel(
        //uuid random
        String unitId,
        //localdatetime.now
        LocalDateTime unitTime,
        //prop
        Meta meta) {
}
